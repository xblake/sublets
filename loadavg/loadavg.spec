# Loadavg specification file
# Created with sur-0.1
Sur::Specification.new do |s|
  s.name        = "Loadavg"
  s.authors     = [ "Christoph Kappel" ]
  s.date        = "Sat Sep 13 19:00 CET 2008"
  s.contact     = "unexist@dorfelite.net"
  s.description = "Show the load average"
  s.version     = "0.2"
  s.tags        = [ "Proc" ]
  s.files       = [ "loadavg.rb" ]
  s.icons       = [ ]
end
