# Freq specification file
# Created with sur-0.1
Sur::Specification.new do |s|
  s.name        = "Freq"
  s.authors     = [ "Christoph Kappel" ]
  s.date        = "Mon May 18 21:00 CET 2009"
  s.contact     = "unexist@dorfelite.net"
  s.description = "Show the cpu frequence"
  s.version     = "0.2"
  s.tags        = [ "Proc", "Multicore" ]
  s.files       = [ "freq.rb" ]
  s.icons       = [ ]
end
